﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSocket4Net;
using SuperSocket.ClientEngine;
using System.Configuration;

namespace QueenPlayOWIN
{
    class WSManager
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        static string port = ConfigurationManager.AppSettings["jpport"];
        static string webSocketUri = ConfigurationManager.AppSettings["jackpot_connection"];
        private static WSManager instance = null;
        public  WebSocket webSocket;
        private static readonly object padlock = new object();

        private WSManager()
        {
            WSInitialazer();
            logger.Info("webSocketUri is " + webSocketUri);
        }

        public static WSManager Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new WSManager();
                    }
                    return instance;
                }
            }
        }
        private void WSInitialazer()
        {
            webSocket = new WebSocket(webSocketUri);
            webSocket.Opened += new EventHandler((object sender, EventArgs e) =>
            {
                logger.Info("Web socket was opened");
            });
            webSocket.Closed += new EventHandler((object sender, EventArgs e) =>
            {
                logger.Info("Web socket was closed");
            });
            webSocket.Error += new EventHandler<ErrorEventArgs>((object sender, ErrorEventArgs e) =>
            {
                logger.Info("Exception");
                logger.Error(e.Exception.Message);
                logger.Info("Web socket will be closed");
                webSocket.Close();
            });
            try
            {
                webSocket.Open();
            }
            catch(Exception ex)
            {
                logger.Error(ex.Message);
            }
        }
    }
}
