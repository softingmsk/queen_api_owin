﻿using DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Owin.Hosting;
using QueenPlayOWIN;
using System.Threading;
using System.Configuration;

namespace QueenOfSpadesService
{
    public partial class QSService : ServiceBase
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        public QSService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {

                logger.Info("OnStart");
                logger.Info("Check tables");
                using (var db = new DBContext())
                {
                    if (db.Bets == null || db.Bets.Where(it => it.Id == 1).FirstOrDefault() == null)
                    {
                        logger.Info("Create table Bets");
                        var newPrinter = db.Bets.Create();
                        newPrinter.Data = "first";
                        db.Bets.Add(newPrinter);
                        db.SaveChanges();
                    }
                    logger.Info("Table Bets exists");

                    if (db.Winner == null || db.Winner.Where(it => it.Id == 1).FirstOrDefault() == null)
                    {
                        logger.Info("Create table Winner");
                        var newWinner = db.Winner.Create();
                        newWinner.WinnerNumber = "34";
                        db.Winner.Add(newWinner);
                        db.SaveChanges();
                    }
                    logger.Info("Table Winner exists");

                    if (db.Status == null || db.Status.Where(it => it.Id == 1).FirstOrDefault() == null)
                    {
                        logger.Info("Create table Status");
                        var new_status = db.Status.Create();
                        new_status.Code = "0";
                        db.Status.Add(new_status);
                        db.SaveChanges();
                    }
                    logger.Info("Table Status exists");
                }

                logger.Info("Owin host initializing");

                string port = ConfigurationManager.AppSettings["port"];
                string ip = ConfigurationManager.AppSettings["ipadress"];

                logger.Info("IP adress of Server: " + ip);
                logger.Info("port: " + port);

                string baseAddress = "http://localhost:" + port;

                StartOptions options = new StartOptions();
                options.Urls.Add(baseAddress);
                options.Urls.Add("http://127.0.0.1:" + port);
                options.Urls.Add(ip + ":" + port);
                options.Urls.Add(string.Format("http://{0}:" + port, Environment.MachineName));

                logger.Info("Start OWIN host");
                WebApp.Start<Startup>(options);
            }
            catch (Exception ex)
            {
                logger.Error("In on Start");
                logger.Error(ex.Message);
                logger.Error(ex.Data);
                logger.Error(ex.InnerException);
                logger.Error(ex.Source);
            }

        }

    protected override void OnStop()
    {
    }
    }
}
