﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace DataBase
{
    public class Status
    {
        public int Id { get; set; }
        [Required]
        public string Code { get; set; }
    }
}
