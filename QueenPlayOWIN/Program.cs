﻿using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using DataBase;
using System.Data.Entity;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Configuration;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Configuration;

namespace QueenPlayOWIN
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var db = new DBContext())
            {
                if(db.Bets == null ||  db.Bets.Where(it => it.Id == 1).FirstOrDefault() == null)
                {
                    var newPrinter = db.Bets.Create();
                    newPrinter.Data = "first";
                    db.Bets.Add(newPrinter);
                    db.SaveChanges();
                }

                if (db.Winner == null || db.Winner.Where(it => it.Id == 1).FirstOrDefault() == null)
                {
                    var newWinner = db.Winner.Create();
                    newWinner.WinnerNumber = "34";
                    db.Winner.Add(newWinner);
                    db.SaveChanges();
                }

                if (db.Status == null || db.Status.Where(it => it.Id == 1).FirstOrDefault() == null)
                {
                    var new_status = db.Status.Create();
                    new_status.Code = "0";
                    db.Status.Add(new_status);
                    db.SaveChanges();
                }

                var ress = db.Winner.Where(it => it.Id == 1).FirstOrDefault();
            }

            string port = ConfigurationManager.AppSettings["port"];        
            string ip = ConfigurationManager.AppSettings["ipadress"];

            string baseAddress = "http://localhost:" + port;
     
            StartOptions options = new StartOptions();
            options.Urls.Add(baseAddress);
            options.Urls.Add("http://127.0.0.1:" + port);
            options.Urls.Add(ip + ":" + port);
            options.Urls.Add(string.Format("http://{0}:" + port, Environment.MachineName));

            // Start OWIN host 
            WebApp.Start<Startup>(options);
            //Console.WriteLine("Go");
            while (true) ;
            /*
            using (WebApp.Start<Startup>(url: baseAddress))
            {
                // Create HttpCient and make a request to api/values 
                System.Net.Http.HttpClient client = new HttpClient();

                var response = client.GetAsync(baseAddress + "api/values").Result;

                Console.WriteLine(response);
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Console.ReadLine();
            }
            */
        }
    }
}
