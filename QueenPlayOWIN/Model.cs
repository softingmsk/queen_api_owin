﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueenPlayOWIN
{
    class Model
    {
    }

    class Bets
    {
       public string[] bets { get; set; }
    }

    class Winner
    {
        public string WinnerNumber { get; set; }
        public string WinnerId { get; set; }
    }

    public class Status
    {
        public string status_code { get; set; }
        public string url_to_show { get; set; }
        public string translation_zone { get; set; }
    }


    public class GamePlayQueen
    {
        public Status QueenOfSpades { get; set; }
    }

    public class GamePlayHill
    {
        public Status KingOfTheHill { get; set; }
    }

    public class RootObjectQueen
    {
        public GamePlayQueen Draw { get; set; }
    }

    public class RootObjectHill
    {
        public GamePlayHill Draw { get; set; }
    }

}
