﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Web.Http;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net.Http.Formatting;
using DataBase;
using WebSocket4Net;
using System.Threading.Tasks;
using SuperSocket.ClientEngine;
using System.Configuration;

namespace QueenPlayOWIN
{
    public class QueenOfSpadesController : ApiController
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();



        public QueenOfSpadesController()
        {
            // to initialize before sending
            var inst = WSManager.Instance;
        }

         [HttpPost]
        public HttpResponseMessage SendData()
        {                    
            HttpContent requestContent = Request.Content;
            Bets data = requestContent.ReadAsAsync<Bets>().Result;

            using (var db = new DBContext())
            {
                string input_data = string.Join("|", data.bets);
                var ress = db.Bets.Where(it => it.Id == 1).FirstOrDefault();
                ress.Data = input_data;
                db.SaveChanges();
            }
                // string jsonContent = requestContent.ReadAsStringAsync().Result;
              //  Console.WriteLine(data.ToString());
          //  Console.WriteLine(data.bets[2]);
            
            HttpResponseMessage response = new HttpResponseMessage();
            response.Headers.Add("Access-Control-Allow-Headers", "x-requested-with");
            return response;
        }

        [HttpGet]
        public HttpResponseMessage GetData()
        {
             Bets output_data = new Bets();
            string data = "";
            using (var db = new DBContext())
            {
                var result = db.Bets.Where(it => it.Id == 1).FirstOrDefault();
                if(result != null)
                {
                    data = result.Data;
                }
            }
            output_data.bets = data.Split('|'); 
            HttpContent requestContent = Request.Content;
            HttpResponseMessage response = new HttpResponseMessage();
            response.Content = new ObjectContent<Bets>(output_data, new JsonMediaTypeFormatter(), "application/json");
            return response;
        }

        /*
 [HttpPut]
 [AllowAnonymous]
 public HttpResponseMessage SendData()
 {
     Console.WriteLine(Request.Content.ToString());
     HttpResponseMessage response = new HttpResponseMessage();
     return response;
 }
 */
        [HttpPost]
        public HttpResponseMessage SendData(string data)
        {
            //Console.WriteLine("data " + data);
            HttpResponseMessage response = new HttpResponseMessage();
            return response;
        }

        [HttpPost]
        public HttpResponseMessage SendWinNumber()
        {
            HttpContent requestContent = Request.Content;
            Winner data = requestContent.ReadAsAsync<Winner>().Result;
            using (var db = new DBContext())
            {
                var ress = db.Winner.Where(it => it.Id == 1).FirstOrDefault();
                ress.WinnerNumber = data.WinnerNumber;
                db.SaveChanges();
            }
            HttpResponseMessage response = new HttpResponseMessage();
            return response;
        }

        [HttpGet]
        public HttpResponseMessage GetWinner()
        {
            Winner output_data = new Winner();
            string winner_number = "";
            string winner_id = "";
            using (var db = new DBContext())
            {
                var result = db.Winner.Where(it => it.Id == 1).FirstOrDefault();
                if (result != null)
                {
                    winner_number = result.WinnerNumber;
                    int num = 0;
                    if(Int32.TryParse(winner_number, out num))
                    {
                        if(num >= 0 && num <= 36)
                        {
                            string ids = "";

                            var resultIds = db.Bets.Where(it => it.Id == 1).FirstOrDefault();
                            if (resultIds != null)
                            {
                                ids = resultIds.Data;
                            }
                            string[] bets;
                            bets = ids.Split('|');
                            winner_id = bets[num];
                        }
                    }
                  
                }
            }
            output_data.WinnerNumber = winner_number;
            output_data.WinnerId = winner_id;
            HttpContent requestContent = Request.Content;
            HttpResponseMessage response = new HttpResponseMessage();
            response.Content = new ObjectContent<Winner>(output_data, new JsonMediaTypeFormatter(), "application/json");
            return response;
        }

        [HttpPost]
        public HttpResponseMessage SetStatus()
        {
            HttpContent requestContent = Request.Content;
            Status data = requestContent.ReadAsAsync<Status>().Result;
            using (var db = new DBContext())
            {
                var ress = db.Status.Where(it => it.Id == 1).FirstOrDefault();
                ress.Code = data.status_code;
                db.SaveChanges();
            }
            
            if(WSManager.Instance.webSocket.State != WebSocketState.Open)
            {
                WSManager.Instance.webSocket.Open();
            }
            if(WSManager.Instance.webSocket.State == WebSocketState.Open)
            {

                RootObjectQueen rootQueen = new RootObjectQueen();
                rootQueen.Draw = new GamePlayQueen();
                rootQueen.Draw.QueenOfSpades = data;
                WSManager.Instance.webSocket.Send(JsonConvert.SerializeObject(rootQueen));
            }
            else
            {
                logger.Info("connection to scala hasn't been opened");
            }
            HttpResponseMessage response = new HttpResponseMessage();
            return response;
        }

        [HttpPost]
        public HttpResponseMessage SetStatusKOH()
        {
            logger.Info("SetStatusKOH");
            HttpContent requestContent = Request.Content;
            Status data = requestContent.ReadAsAsync<Status>().Result;
            logger.Info("data: " + data.status_code + " " 
                + data.translation_zone + " " + data.url_to_show);
            using (var db = new DBContext())
            {
                var ress = db.Status.Where(it => it.Id == 1).FirstOrDefault();
                ress.Code = data.status_code;
                db.SaveChanges();
            }

            if (WSManager.Instance.webSocket.State != WebSocketState.Open)
            {
                WSManager.Instance.webSocket.Open();
            }
            if (WSManager.Instance.webSocket.State == WebSocketState.Open)
            {

                RootObjectHill rootQueen = new RootObjectHill();
                rootQueen.Draw = new GamePlayHill();
                rootQueen.Draw.KingOfTheHill = data;
                WSManager.Instance.webSocket.Send(JsonConvert.SerializeObject(rootQueen));
            }
            else
            {
                logger.Info("connection to scala hasn't been opened");
            }
            HttpResponseMessage response = new HttpResponseMessage();
            return response;
        }


        [HttpGet]
        public HttpResponseMessage GetStatus()
        {
            Status output_data = new Status();
            string data = "";
            using (var db = new DBContext())
            {
                var result = db.Status.Where(it => it.Id == 1).FirstOrDefault();
                if (result != null)
                {
                    data = result.Code;
                }
            }
            output_data.status_code = data;
            HttpContent requestContent = Request.Content;
            HttpResponseMessage response = new HttpResponseMessage();
            response.Content = new ObjectContent<Status>(output_data, new JsonMediaTypeFormatter(), "application/json");
            return response;
        }

    }
}
